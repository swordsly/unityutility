﻿using UnityEngine.UI;

namespace UnityUtility {
	public class EmptyGraphic : Graphic {
		protected override void OnPopulateMesh (VertexHelper vh) {
			vh.Clear();
		}
	}
}