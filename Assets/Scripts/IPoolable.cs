namespace UnityUtility {
    public interface IPoolable {
        int PoolKey { get; set; }
    }
}