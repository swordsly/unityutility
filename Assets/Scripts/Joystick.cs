﻿using UnityEngine;
using UnityEngine.EventSystems;
using System;

namespace UnityUtility {
    public class Joystick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler {

        #region Public Functions
        public void OnPointerDown (PointerEventData data) {
            canvasGroup.alpha = (hideOnRelease) ? 1f : 0f;
            if (snapToFinger) {
                Vector3 _worldPosition;
                RectTransformUtility.ScreenPointToWorldPointInRectangle(rect: stickRect, screenPoint: data.position, cam: data.pressEventCamera, worldPoint: out _worldPosition);
                stickRect.position = baseRect.position = _worldPosition;
            }
            
            if (mOnDownEvent != null)
                mOnDownEvent(baseRect.position);
        }

        public void OnPointerUp (PointerEventData data) {
            stickRect.anchoredPosition = mAxisValue = Vector2.zero;
            canvasGroup.alpha = (hideOnRelease) ? 0f : 1f;

            if (mOnUpEvent != null)
                mOnUpEvent();
        }

        public void OnDrag (PointerEventData data) {
            Vector3 _worldPosition;
            RectTransformUtility.ScreenPointToWorldPointInRectangle(rect: stickRect, screenPoint: data.position, cam: data.pressEventCamera, worldPoint: out _worldPosition);
            stickRect.position = _worldPosition;
            
            Vector2 _difference = stickRect.anchoredPosition - baseRect.anchoredPosition;
            float _differenceMagnitudeSqr = _difference.sqrMagnitude;
            if (_differenceMagnitudeSqr > mMovementRangeSqr)
                stickRect.anchoredPosition = baseRect.anchoredPosition + _difference.normalized * movementRange;
            
            _difference = stickRect.anchoredPosition - baseRect.anchoredPosition;
            mAxisValue = new Vector2(Mathf.Clamp(_difference.x * mOneOverMovementRange, -1f, 1f), Mathf.Clamp(_difference.y * mOneOverMovementRange, -1f, 1f));

            Vector2 _signs = new Vector2(Mathf.Sign(mAxisValue.x), Mathf.Sign(mAxisValue.y));
            mAxisValue = new Vector2((float) Math.Round(_signs.x * sensitiveCurve.Evaluate(_signs.x * mAxisValue.x), 2), 
                                    (float) Math.Round(_signs.y * sensitiveCurve.Evaluate(_signs.y * mAxisValue.y), 2));
            
            if (mOnDragEvent != null)
                mOnDragEvent(mAxisValue, mAxisValue.sqrMagnitude >= deadZone);
        }
        #endregion
        
        #region MonoBehaviour Functions
        void Awake () {
            canvasGroup.alpha = (hideOnRelease) ? 0f : 1f;
            mMovementRangeSqr = movementRange * movementRange;
            mOneOverMovementRange = 1f / movementRange;
        }
        #endregion

        #region Delegates, Events
        event Action<Vector2, bool> mOnDragEvent;
        public void SubscribeToDragEvent (Action<Vector2, bool> subscriber) { mOnDragEvent += subscriber; }
        public void UnsubscribeToDragEvent (Action<Vector2, bool> subscriber) { mOnDragEvent -= subscriber; }

        event Action mOnUpEvent;
        public void SubscribeToUpEvent (Action subscriber) { mOnUpEvent += subscriber; }
        public void UnsubscribeToUpEvent (Action subscriber) { mOnUpEvent -= subscriber; }

        event Action<Vector3> mOnDownEvent;
        public void SubscribeToDownEvent (Action<Vector3> subscriber) { mOnDownEvent += subscriber; }
        public void UnsubscribeToDownEvent (Action<Vector3> subscriber) { mOnDownEvent -= subscriber; }
        #endregion

        #region Inspector Variables
        [Header("Settings")]
        [Tooltip("Max distance away from base (screen space unit)")]
        [SerializeField, Range(0, 100)] float movementRange = 50f;
        [Tooltip("Axis Value sqrMag lesser than this value will be invalid")]
        [SerializeField, Range(0, 1)] float deadZone = 0.5f;
        [Space(5f)]
        [SerializeField] bool hideOnRelease;
        [SerializeField] bool snapToFinger;
        [Space(5f)]
        [SerializeField] AnimationCurve sensitiveCurve = new AnimationCurve(new Keyframe(0f, 0f), new Keyframe(1f, 1f));

        [Header("References")]
        [SerializeField] RectTransform baseRect;
        [SerializeField] RectTransform stickRect;
        [SerializeField] CanvasGroup canvasGroup;
        #endregion

        #region Fields, Accessors
        float mMovementRangeSqr;
        float mOneOverMovementRange;

        Vector2 mAxisValue;
        public Vector2 AxisValue { get { return mAxisValue; } }
        #endregion
    }
}