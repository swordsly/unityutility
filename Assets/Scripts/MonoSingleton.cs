﻿using UnityEngine;
using Sirenix.OdinInspector;

namespace UnityUtility {
    public class MonoSingleton<T> : SerializedMonoBehaviour where T : MonoBehaviour {
        
        #region MonoBehaviour Functions
        void Awake() {
            Instance = this as T;
        }
        #endregion

        #region Fields, Accessors
        static T instance;
        public static T Instance {
            get {
                if (instance == null) {
                    instance = FindObjectOfType<T>();
                    if (instance == null) {
                        GameObject _singleton = new GameObject();
                        instance = _singleton.AddComponent<T>();
                    }
                }
                return instance;
            }
            private set {
                if (instance != null)
                    return;
                
                instance = value;
                instance.gameObject.name = "_" + typeof(T).Name + "_";
                DontDestroyOnLoad(instance);
            }
        }
        #endregion
    }
}