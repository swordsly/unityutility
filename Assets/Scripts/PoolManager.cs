﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityUtility {
    public class PoolManager : MonoSingleton<PoolManager> {
        #region Public Functions
        public bool GetPool (int poolKey, out UnityPool pool) {
            bool _result = mPools.ContainsKey(poolKey);
            if (_result)
                pool = mPools[poolKey];
            else
                pool = null;
            return _result;
        }

        public bool SetupPool (GameObject prefab, int size, int forceCleanThreshold, out int poolKey) {
            poolKey = prefab.GetHashCode();
            if (!mPools.ContainsKey(poolKey)) {   
                mPools.Add(poolKey, new UnityPool(prefab, size, forceCleanThreshold, poolKey));
                return true;
            }
            return false;
        }

        public bool DestroyPool (GameObject prefab) {
            int _poolKey = prefab.GetHashCode();
            if (!mPools.ContainsKey(_poolKey)) {
                mPools[_poolKey].DestroyPool();
                mPools[_poolKey] = null;
                return mPools.Remove(_poolKey);
            }
            return false;
        }
        #endregion

        #region MonoBehaviour Functions
        void Awake () {
            mTransform = transform;
            mPools = new Dictionary<int, UnityPool>();
        }
        #endregion

        #region Fields, Accessors
        Transform mTransform;
        public Transform CachedTransform {
            get {
                return mTransform;
            }
        }

        Dictionary<int, UnityPool> mPools;
        #endregion
    }
}