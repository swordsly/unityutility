﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityUtility {
    public class UnityPool {
        #region Public Functions, Initialiser
        public GameObject GetObject () {
            if (mCleanObjects.Peek() != null)
                return mCleanObjects.Pop();
            
            if (!mIsCleaning && mDirtyObjects.Peek() != null) {
                GameObject _object = mDirtyObjects.Pop();
                _object.GetComponent<IResettable>().ResetThis();
                return _object;
            }
            
            GameObject _clone = GameObject.Instantiate(mBasePrefab, Vector3.zero, Quaternion.identity, PoolManager.Instance.CachedTransform);
            IPoolable _poolable;
            if (_clone.TryGetComponent<IPoolable>(out _poolable))
                _poolable.PoolKey = mPoolKey;
            _clone.SetActive(false);
            return _clone;
        }

        public void ReturnObject (GameObject dirtyObject) {
            if (dirtyObject == null)
                return;
            
            dirtyObject.SetActive(false);
            mDirtyObjects.Push(dirtyObject);

            if (!mIsCleaning && mDirtyObjects.Count >= mForceCleanThreshold)
                PoolManager.Instance.StartCoroutine(DirtyToCleanRoutine());
        }

        public UnityPool (GameObject prefab, int size, int forceCleanThreshold, int poolKey) {
            mBasePrefab = prefab;
            mInitialSize = size;
            mForceCleanThreshold = forceCleanThreshold;
            mPoolKey = poolKey;

            mCleanObjects = new Stack<GameObject>(size);
            mDirtyObjects = new Stack<GameObject>(forceCleanThreshold);

            for (int i = 0; i < size; i++) {
                GameObject _clone = GameObject.Instantiate(prefab, Vector3.zero, Quaternion.identity, PoolManager.Instance.CachedTransform);
                IPoolable _poolable;
                if (_clone.TryGetComponent<IPoolable>(out _poolable))
                    _poolable.PoolKey = poolKey;
                _clone.SetActive(false);
                mCleanObjects.Push(_clone);
            }
        }

        public void DestroyPool () {
            for (int i = 0; i < mCleanObjects.Count; i++)
                GameObject.Destroy(mCleanObjects.Pop());
            for (int i = 0; i < mDirtyObjects.Count; i++)
                GameObject.Destroy(mDirtyObjects.Pop());

            mCleanObjects = null;
            mDirtyObjects = null;
            mBasePrefab = null;
        }
        #endregion

        #region Private Functions
        IEnumerator DirtyToCleanRoutine () {
            mIsCleaning = true;
            int _count = mDirtyObjects.Count;
            do {
                GameObject _object = mDirtyObjects.Pop();
                _object.GetComponent<IResettable>().ResetThis();
                mCleanObjects.Push(_object);
                _count--;
                yield return null;
            } while (_count > 0);
            mIsCleaning = false;
        }
        #endregion

        #region Fields, Accessors
        Stack<GameObject> mCleanObjects;
        Stack<GameObject> mDirtyObjects;
        
        GameObject mBasePrefab;
        int mInitialSize;
        int mForceCleanThreshold;
        int mPoolKey;

        bool mIsCleaning;
        #endregion
    }
}